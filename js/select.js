var jsondata;
var imgid = [];
var imgurl = [];
var img;
var sentry_id;
var days_back;
var timestamp;
var inurl;
var outurl
var op = {};

function getThirty() {
	timestamp = Math.floor(Date.now() / 1000) - 86400 * 30;
	timestamp = "db_"+timestamp;
	console.log(timestamp);
	getdata();
}

function getTimestamp() {
	timestamp = document.getElementById("timestamp").value;
	getdata();
}

function getdata() {


	sentry_id = document.getElementById("sentry_id").value;
	// days_back = document.getElementById("days_back").value;
	

	document.getElementById("found").innerHTML = "<h1>Loading Images</h1>";

	inurl = "https://kmr0fi5yfd.execute-api.us-west-2.amazonaws.com/v1/select?sentry_id="+sentry_id+"&timestamp="+timestamp;
	console.log(inurl)

	request = new XMLHttpRequest;
	request.open('GET', inurl, true);

	request.onload = function() {
	  if (request.status >= 200 && request.status < 400){
	    // Success!
	    jsondata = JSON.parse(request.responseText);
	    imgid = Object.keys(jsondata);
	    imgurl = Object.values(jsondata);
	    document.getElementById("found").innerHTML = "<h1>Fetched "+imgurl.length+" Images </h1>";

		for (var i = 0; i < imgid.length; i++) {

			img = new Image();
			img.src = imgurl[i];
			var el = document.querySelector('form');
			el.innerHTML += '<div class="gallery"><input type="checkbox" id="in_'+imgid[i]+'"/><label class="btn" for="in_'+imgid[i]+'" id="'+imgid[i]+'"><div class="desc">'+imgid[i]+'</div></label></div>';
			var parent = document.getElementById(imgid[i]);
			parent.append(img);
			console.log("get");
		}
	  } else {
	    // We reached our target server, but it returned an error
	    	console.log("error");
	  }
	};

	request.onerror = function() {
	  // There was a connection error of some sort
	};

	request.send();
}

function senddata() {

	console.clear();
	for (var i = 0; i < imgid.length; i++) {
		if(document.getElementById("in_"+imgid[i]).checked) {
			op[imgid[i]] = imgurl[i];
		}
	}
	var optemp = {"event_data" : op};
	var payload = JSON.stringify(optemp);
	console.log(payload);

	outurl = "https://kmr0fi5yfd.execute-api.us-west-2.amazonaws.com/v1/select"
	var outrequest = new XMLHttpRequest();
	outrequest.open("POST",outurl,true);
	outrequest.setRequestHeader('Content-type','application/json; charset=utf-8');
	outrequest.send(payload);
	document.getElementById("done").innerHTML = "SUBMITTED"
}

